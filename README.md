# Ejemplo sencillo de *Hello World* en HTTP para probar un balanceador de carga

Esta imagen de docker permite crear un servidor HTTP para probar un balanceador de carga. 

El servidor HTTP por cada petición que reciba (GET) mostrará el hostname del servidor que atendió la petición.

## Modo de uso

Con la siguiente instrucción se creará un contenedor a partir de la imagen de este repositorio.

``` 
docker run --rm -it -p 80:80 marcelocapozzi/hello-world-http
```

Luego de la ejecución, el servidor quedará creado y esperando peticiones.

Para corroborar el funcionamiento se puede acceder via web a la instancia del contenedor creada recientemente.

- Desde un navegador el resultado esperado será el siguiente:

  ![output web browser!](/images/output-w.png "output web browser")

- Desde el servidor, la petición anterior se verá de la siguiente manera:

  ![output terminal!](/images/output-t.png "output terminal")

> **Repositorio**: [**hello-world-http**](https://gitlab.com/MarceloCapozzi/hello-world-http)
